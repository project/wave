This is a module to provide google wave robot a communication way with drupal.
Ways to call wave.articlesave xmlrpc method:
<code>$account = 'yangru@wavesandbox.com'; 
  $article = array('title'=>'aaasss', 'body'=>'bbb');
  $result = xmlrpc('http://10.10.10.105/drupal6/xmlrpc', 'drubot.save', $account, md5($account . '123456789'), 'wavesandbox+!123i45ddfai', $article);
  print_r($result);</code>

Installation steps:
1.Enable this module 
  Note: you need to enable cck and profile first before you enable the wave module, or you will fail to add cck fields and add profile fields  when installation. When this happens, just uninstall and install it will be ok.
2.Go to admin/settings/wave, configure everything there.
